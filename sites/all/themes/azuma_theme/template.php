<?php

/**
 * Preprocessor for page.tpl.php template file.
 */
function azuma_theme_preprocess_page(&$vars, $hook) {

  // For easy printing of variables.
  $vars['logo_img'] = '';
  if (!empty($vars['logo'])) {
    $vars['logo_img'] = theme('image', array(
      'path'  => $vars['logo'],
      'alt'   => t('Home'),
      'title' => t('Home'),
    ));
  }
  $vars['linked_logo_img']  = '';
  if (!empty($vars['logo_img'])) {
    $vars['linked_logo_img'] = l($vars['logo_img'], '<front>', array(
      'attributes' => array(
        'rel'   => 'home',
        'title' => t('Home'),
      ),
      'html' => TRUE,
    ));
  }
  $vars['linked_site_name'] = '';
  if (!empty($vars['site_name'])) {
    $vars['linked_site_name'] = l($vars['site_name'], '<front>', array(
      'attributes' => array(
        'rel'   => 'home',
        'title' => t('Home'),
      ),
    ));
  }

  // Site navigation links.
  $vars['main_menu_links'] = '';
  if (isset($vars['main_menu'])) {
    $vars['main_menu_links'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'id' => 'main-menu',
        'class' => array('inline', 'main-menu'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  $vars['secondary_menu_links'] = '';
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_menu_links'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'id'    => 'secondary-menu',
        'class' => array('inline', 'secondary-menu'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
}

/**
 * Textarea preprocess.
 */
function azuma_theme_textarea($element) {
  $element['element']['#resizable'] = FALSE;
  return theme_textarea($element);
}

/**
 * Implements template_preprocess_views_view_table.
 */
function azuma_theme_preprocess_views_view_table(&$vars) {
  if ($vars['view']->name == 'entity_node') {
    if ($vars['view']->current_display == 'trades_list') {
      // @TODO: Need a better solution here.
      // Count profit from trade operation.
      $fields = array('amount', 'price_seller', 'price_buyer');
      $total_profit = 0;
      foreach ($vars['rows'] as $key => $row) {
        // Rollback output.
        if (empty($vars['rows'][$key]['field_rollback'])) {
          // Fetch fields.
          foreach ($fields as $field_suffix) {
            $field = 'field_trade_' . $field_suffix;
            $$field_suffix = intval($row[$field]);
          }
          $total_profit += $price_buyer * $amount - $price_seller * $amount;
        }
      }
      // @TODO: Need a better solution here.
      // Output value.
      $vars['title'] = format_string($vars['title'], array('!profit' => t('Profit: !profit hrn.', array('!profit' => $total_profit))));
    }
  }
}
