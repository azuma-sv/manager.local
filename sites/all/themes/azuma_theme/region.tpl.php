<?php
/**
 * @file
 * Default theme implementation to display a region.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 */
?>

<?php if ($content): ?>
  <?php print $content; ?>
<?php endif; ?>
