<?php
/**
 * @file
 * Main page HTML layout.
 */

?>
<?php if (!empty($page['header'])) : ?>
  <div class="site-header">
    <div class="container clearfix">
      <div class="col-xs-12">
        <?php print render($page['header']); ?>
      </div>
    </div>
  </div>
<?php endif; ?>
<div class="site-content">
  <div class="container clearfix">
      <div class="page-header col-xs-12 clearfix">
        <?php print $breadcrumb; ?>

        <?php print render($title_prefix); ?>
        <?php if (!empty($title)): ?>
          <h1 class="page-title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>

        <?php if (!empty($tabs)): ?>
          <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>

        <?php print $messages; ?>
      </div>

    <div class="page-content col-xs-12 clearfix">
      <?php if (!empty($page['content'])) : ?>
        <?php print render($page['content']); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php if (!empty($page['footer'])) : ?>
  <div class="site-footer">
    <div class="container clearfix">
      <div class="col-xs-12">
        <?php print render($page['footer']); ?>
      </div>
    </div>
  </div>
<?php endif; ?>
