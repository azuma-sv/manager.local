(function ($) {
  Drupal.behaviors.formAppearance = {
    attach: function (context, settings) {
      try {
        // Replace all select lists with custom plugin.
        $selects = $('select:not(lang-dropdown-select-element)');
        $selects.msDropDown({
          width: 300,
          roundedCorner: true,
          event: 'click',
          mainCSS: 'aSkin'
        });

        // Processing select with.
        $newSelects = $('.aSkin');
        $newSelects.each(function(){
          var $this = $(this),
              width = 36, itemWidth;
          $this.find('.ddlabel').each(function(){
            // Calculate text width.
            var $clone = $(this).clone();
            $('body').append($clone);
            // Include paddings
            itemWidth = $clone.outerWidth() + 16;
            if (width < itemWidth) {
              // Fetch biggest width.
              width = itemWidth;
            }
            $clone.remove();
          });
          // Set select width (include selectlist paddings and borders).
          $this.css('width', width + 44);
          // Set validation error class.
          var $originalSelect = $this.prev().find('select').first();
          if ($originalSelect.hasClass('error')) {
            $this.addClass('error');
          }

        });
      }
      catch (e) {
        if (console) { console.log(e); }
      }
    }
  };

  Drupal.behaviors.formConditionalFields = {
    attach: function (context, settings) {
      // Function to update fields visibility.
      var updateFieldsVisibility = function ($collapsible, state, speed) {
        // Hide or show fields if we need that.
        if (!state) {
          $collapsible.slideUp(speed);
        }
        else {
          $collapsible.slideDown(speed, function () {
            $collapsible.css('overflow', 'visible');
          });
        }
      }

      // Fetch triggering field.
      $('.field-name-field-rollback .form-checkbox').first().once('once-conditional', function () {
        var $this = $(this);
        // Fields which should collapse.
        $this.collapsibleChildren = $('.field-name-field-rollback-type, .field-name-field-description');
        // If default value was inserted.
        updateFieldsVisibility($this.collapsibleChildren, $this.prop('checked'), 0);
        // Triggering event "on change".
        $this.change(function () {
          updateFieldsVisibility($this.collapsibleChildren, $this.prop('checked'), 500);
        });
      });

      // Fetch triggering field.
      $('.field-name-field-rollback-type .form-select').first().once('once-conditional', function () {
        var $this = $(this);
        // Fields which should collapse.
        $this.collapsibleChildren = $('.field-name-field-contractor');
        // If default value was inserted.
        $this.getState = function () { return $this.val() == 'retrade' || $this.val() == 'store'; };
        updateFieldsVisibility($this.collapsibleChildren, $this.getState(), 0);
        // Triggering event "on change".
        $this.change(function () {
          updateFieldsVisibility($this.collapsibleChildren, $this.getState(), 500);
        });
        // Clear this field when rollback changed.
        $('.field-name-field-rollback .form-checkbox').first().change(function () {
          $this.find('option').each(function () {
            this.selected = (this.value == '_none');
          });
          $this.change();
        });
      });

      // Fetch triggering field.
      $('.field-name-field-status .form-select').first().once('once-conditional', function () {
        var $this = $(this);
        // Fields which should collapse.
        $this.collapsibleChildren = $('.field-name-field-name, .field-name-field-fathers-name');
        // If default value was inserted.
        updateFieldsVisibility($this.collapsibleChildren, $this.val() != 'enterprise', 0);
        // Triggering event "on change".
        $this.change(function () {
          // Hide or show fields if we need that.
          updateFieldsVisibility($this.collapsibleChildren, $this.val() != 'enterprise', 500);
        });
      });
    }
  };
})(jQuery);
