<?php

// Plugin definition
$plugin = array(
  'title' => t('Default layout'),
  'category' => t('Default'),
  'icon' => 'default_layout.png',
  'theme' => 'default_layout',
  'css' => 'default_layout.css',
  'regions' => array(
    'top' => t('Top'),
    'content' => t('Content'),
    'right' => t('Right sidebar'),
    'bottom' => t('Bottom'),
  ),
);
