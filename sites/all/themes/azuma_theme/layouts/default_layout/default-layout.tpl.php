<?php
/**
 * @file
 * Template for a 2 column panel layout.
 */

$content_width = empty($content['right']) ? 'col-xs-12' : 'col-xs-8';
$right_width = empty($content['content']) ? 'col-xs-12' : 'col-xs-4';

?>
<div class="panel-layout-default clearfix">
  <?php if (!empty($content['top'])) : ?>
    <div class="panel-top clearfix">
      <?php print render($content['top']); ?>
    </div>
  <?php endif; ?>

  <div class="panel-content-wrapper clearfix">
    <div class="row">
      <?php if (!empty($content['content'])) : ?>
        <div class="panel-content <?php print $content_width; ?> clearfix">
          <?php print render($content['content']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($content['right'])) : ?>
        <div class="panel-right <?php print $right_width; ?> clearfix">
          <?php print render($content['right']); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <?php if (!empty($content['bottom'])) : ?>
    <div class="panel-bottom clearfix">
      <?php print render($content['bottom']); ?>
    </div>
  <?php endif; ?>
</div>
