<?php
/**
 * @file
 * Template for a one column panel layout.
 */

?>

<div class="panel-layout-singlecol clearfix">
  <?php if (!empty($content['content'])) : ?>
    <?php print render($content['content']); ?>
  <?php endif; ?>
</div>
