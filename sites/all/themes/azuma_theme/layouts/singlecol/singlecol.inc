<?php

// Plugin definition
$plugin = array(
  'title' => t('One column layout'),
  'category' => t('Default'),
  'icon' => 'singlecol.png',
  'theme' => 'singlecol',
  'css' => 'singlecol.css',
  'regions' => array(
    'content' => t('Content'),
  ),
);
