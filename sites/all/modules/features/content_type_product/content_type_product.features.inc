<?php
/**
 * @file
 * content_type_product.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_product_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_type_product_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Продукт'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Номер'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
