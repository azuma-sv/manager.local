<?php
/**
 * @file
 * taxonomy_vocabulary_contractor_type.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function taxonomy_vocabulary_contractor_type_taxonomy_default_vocabularies() {
  return array(
    'contractor_type' => array(
      'name' => 'Тип Контрагента',
      'machine_name' => 'contractor_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
  );
}
