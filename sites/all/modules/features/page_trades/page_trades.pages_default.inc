<?php
/**
 * @file
 * page_trades.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function page_trades_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'trades';
  $page->task = 'page';
  $page->admin_title = 'Trades';
  $page->admin_description = '';
  $page->path = 'trades';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_trades__panel';
  $handler->task = 'page';
  $handler->subtask = 'trades';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Trades',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'default_layout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '92d69ef0-35f6-4750-bad3-ea3d459a3752';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c11c2df1-0aa2-459c-9e52-4a26c83870e4';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'entity_node-trades_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '10',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c11c2df1-0aa2-459c-9e52-4a26c83870e4';
    $display->content['new-c11c2df1-0aa2-459c-9e52-4a26c83870e4'] = $pane;
    $display->panels['content'][0] = 'new-c11c2df1-0aa2-459c-9e52-4a26c83870e4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['trades'] = $page;

  return $pages;

}
