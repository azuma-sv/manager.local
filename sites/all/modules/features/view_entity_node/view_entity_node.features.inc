<?php
/**
 * @file
 * view_entity_node.features.inc
 */

/**
 * Implements hook_views_api().
 */
function view_entity_node_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
