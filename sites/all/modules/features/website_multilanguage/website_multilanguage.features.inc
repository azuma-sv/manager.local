<?php
/**
 * @file
 * website_multilanguage.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function website_multilanguage_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
