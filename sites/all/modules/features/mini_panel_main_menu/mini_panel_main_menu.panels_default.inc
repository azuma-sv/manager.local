<?php
/**
 * @file
 * mini_panel_main_menu.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function mini_panel_main_menu_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'main_menu';
  $mini->category = '';
  $mini->admin_title = 'Main menu';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'default_layout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '816735b0-a563-4c17-af78-263043048cf1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-74fcec95-7f8f-493d-b377-55567faa5862';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '74fcec95-7f8f-493d-b377-55567faa5862';
    $display->content['new-74fcec95-7f8f-493d-b377-55567faa5862'] = $pane;
    $display->panels['top'][0] = 'new-74fcec95-7f8f-493d-b377-55567faa5862';
    $pane = new stdClass();
    $pane->pid = 'new-4466613b-5b56-4f86-b430-b52e88ee9647';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-create';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4466613b-5b56-4f86-b430-b52e88ee9647';
    $display->content['new-4466613b-5b56-4f86-b430-b52e88ee9647'] = $pane;
    $display->panels['top'][1] = 'new-4466613b-5b56-4f86-b430-b52e88ee9647';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-74fcec95-7f8f-493d-b377-55567faa5862';
  $mini->display = $display;
  $export['main_menu'] = $mini;

  return $export;
}
