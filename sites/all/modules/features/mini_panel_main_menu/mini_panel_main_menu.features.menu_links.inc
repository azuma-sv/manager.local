<?php
/**
 * @file
 * mini_panel_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function mini_panel_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_:<front>.
  $menu_links['main-menu_:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Домівка',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_:contractors.
  $menu_links['main-menu_:contractors'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'contractors',
    'router_path' => 'contractors',
    'link_title' => 'Контрагенти',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_:contractors',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_:import.
  $menu_links['main-menu_:import'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'import',
    'router_path' => 'import',
    'link_title' => 'Імпорт',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_:import',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_:products.
  $menu_links['main-menu_:products'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'products',
    'router_path' => 'products',
    'link_title' => 'Продукти',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_:products',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_:trades.
  $menu_links['main-menu_:trades'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'trades',
    'router_path' => 'trades',
    'link_title' => 'Продажі',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_:trades',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-create_-:taxonomy/contractor_type/add.
  $menu_links['menu-create_-:taxonomy/contractor_type/add'] = array(
    'menu_name' => 'menu-create',
    'link_path' => 'taxonomy/contractor_type/add',
    'router_path' => 'taxonomy/%/add',
    'link_title' => 'Тип Контрагента',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-create_-:taxonomy/contractor_type/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-create_:node/add',
  );
  // Exported menu link: menu-create_:admin/people/create.
  $menu_links['menu-create_:admin/people/create'] = array(
    'menu_name' => 'menu-create',
    'link_path' => 'admin/people/create',
    'router_path' => 'admin/people/create',
    'link_title' => 'Менеджер',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-create_:admin/people/create',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-create_:node/add',
  );
  // Exported menu link: menu-create_:node/add.
  $menu_links['menu-create_:node/add'] = array(
    'menu_name' => 'menu-create',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Створити',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-create_:node/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-create_:node/add/contractor.
  $menu_links['menu-create_:node/add/contractor'] = array(
    'menu_name' => 'menu-create',
    'link_path' => 'node/add/contractor',
    'router_path' => 'node/add/contractor',
    'link_title' => 'Контрагент',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-create_:node/add/contractor',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-create_:node/add',
  );
  // Exported menu link: menu-create_:node/add/product.
  $menu_links['menu-create_:node/add/product'] = array(
    'menu_name' => 'menu-create',
    'link_path' => 'node/add/product',
    'router_path' => 'node/add/product',
    'link_title' => 'Продукт',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-create_:node/add/product',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-create_:node/add',
  );
  // Exported menu link: menu-create_:taxonomy/brand/add.
  $menu_links['menu-create_:taxonomy/brand/add'] = array(
    'menu_name' => 'menu-create',
    'link_path' => 'taxonomy/brand/add',
    'router_path' => 'taxonomy/%/add',
    'link_title' => 'Бренд',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-create_:taxonomy/brand/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-create_:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Імпорт');
  t('Бренд');
  t('Домівка');
  t('Контрагент');
  t('Контрагенти');
  t('Менеджер');
  t('Продажі');
  t('Продукт');
  t('Продукти');
  t('Створити');
  t('Тип Контрагента');

  return $menu_links;
}
