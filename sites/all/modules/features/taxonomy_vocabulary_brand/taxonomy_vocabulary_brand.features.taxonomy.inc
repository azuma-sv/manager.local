<?php
/**
 * @file
 * taxonomy_vocabulary_brand.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function taxonomy_vocabulary_brand_taxonomy_default_vocabularies() {
  return array(
    'brand' => array(
      'name' => 'Бренд',
      'machine_name' => 'brand',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
  );
}
