<?php
/**
 * @file
 * taxonomy_vocabulary_brand.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function taxonomy_vocabulary_brand_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_settings_taxonomy_term__brand';
  $strongarm->value = array(
    'default_language' => 'xx-et-default',
    'hide_language_selector' => 1,
    'exclude_language_none' => 1,
    'lock_language' => 1,
    'shared_fields_original_only' => 1,
  );
  $export['entity_translation_settings_taxonomy_term__brand'] = $strongarm;

  return $export;
}
