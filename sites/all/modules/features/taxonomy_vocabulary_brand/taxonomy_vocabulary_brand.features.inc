<?php
/**
 * @file
 * taxonomy_vocabulary_brand.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function taxonomy_vocabulary_brand_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
