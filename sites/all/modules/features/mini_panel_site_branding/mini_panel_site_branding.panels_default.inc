<?php
/**
 * @file
 * mini_panel_site_branding.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function mini_panel_site_branding_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'site_branding';
  $mini->category = '';
  $mini->admin_title = 'Site branding';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'default_layout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '87ed3b37-9882-4240-a0dc-8f880f485199';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d6b344fb-5698-4c6f-bfd0-97d4bf37a6c4';
    $pane->panel = 'top';
    $pane->type = 'page_logo';
    $pane->subtype = 'page_logo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'logo',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd6b344fb-5698-4c6f-bfd0-97d4bf37a6c4';
    $display->content['new-d6b344fb-5698-4c6f-bfd0-97d4bf37a6c4'] = $pane;
    $display->panels['top'][0] = 'new-d6b344fb-5698-4c6f-bfd0-97d4bf37a6c4';
    $pane = new stdClass();
    $pane->pid = 'new-9ef69313-7b2b-474b-866c-2e44ce038879';
    $pane->panel = 'top';
    $pane->type = 'page_site_name';
    $pane->subtype = 'page_site_name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'linked' => 1,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'site-name',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9ef69313-7b2b-474b-866c-2e44ce038879';
    $display->content['new-9ef69313-7b2b-474b-866c-2e44ce038879'] = $pane;
    $display->panels['top'][1] = 'new-9ef69313-7b2b-474b-866c-2e44ce038879';
    $pane = new stdClass();
    $pane->pid = 'new-c2b71771-92c4-4e0e-a0c1-50378c5a1a8f';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'lang_dropdown-language';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'lang-dropdown-switcher',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c2b71771-92c4-4e0e-a0c1-50378c5a1a8f';
    $display->content['new-c2b71771-92c4-4e0e-a0c1-50378c5a1a8f'] = $pane;
    $display->panels['top'][2] = 'new-c2b71771-92c4-4e0e-a0c1-50378c5a1a8f';
    $pane = new stdClass();
    $pane->pid = 'new-8dc8f5d9-f4d7-4c7d-a3b4-2c7ea86dd054';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'system-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '8dc8f5d9-f4d7-4c7d-a3b4-2c7ea86dd054';
    $display->content['new-8dc8f5d9-f4d7-4c7d-a3b4-2c7ea86dd054'] = $pane;
    $display->panels['top'][3] = 'new-8dc8f5d9-f4d7-4c7d-a3b4-2c7ea86dd054';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-d6b344fb-5698-4c6f-bfd0-97d4bf37a6c4';
  $mini->display = $display;
  $export['site_branding'] = $mini;

  return $export;
}
