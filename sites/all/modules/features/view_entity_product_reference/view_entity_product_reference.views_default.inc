<?php
/**
 * @file
 * view_entity_product_reference.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function view_entity_product_reference_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'entity_product_reference';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'product_reference';
  $view->human_name = 'Entity: Product reference';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Продукти';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Примінити';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Скинути';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'table';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Нічого не знайдено';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Field: Product reference: Product reference ID (prid) */
  $handler->display->display_options['fields']['prid']['id'] = 'prid';
  $handler->display->display_options['fields']['prid']['table'] = 'product_reference';
  $handler->display->display_options['fields']['prid']['field'] = 'prid';
  $handler->display->display_options['fields']['prid']['label'] = 'PRID';

  /* Display: Contractor products list */
  $handler = $view->new_display('page', 'Contractor products list', 'contractor_products_list');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Продукти Контрагента';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'prid' => 'prid',
    'field_title_et' => 'field_title_et',
    'title' => 'title',
    'field_brand' => 'field_brand',
    'amount' => 'amount',
    'nothing' => 'nothing',
    'nothing_1' => 'nothing_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'prid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_title_et' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_brand' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'amount' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Product reference: Product ID */
  $handler->display->display_options['relationships']['product']['id'] = 'product';
  $handler->display->display_options['relationships']['product']['table'] = 'product_reference';
  $handler->display->display_options['relationships']['product']['field'] = 'product';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Product reference: Product reference ID (prid) */
  $handler->display->display_options['fields']['prid']['id'] = 'prid';
  $handler->display->display_options['fields']['prid']['table'] = 'product_reference';
  $handler->display->display_options['fields']['prid']['field'] = 'prid';
  $handler->display->display_options['fields']['prid']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['prid']['exclude'] = TRUE;
  /* Field: Product reference: Contractor ID */
  $handler->display->display_options['fields']['contractor']['id'] = 'contractor';
  $handler->display->display_options['fields']['contractor']['table'] = 'product_reference';
  $handler->display->display_options['fields']['contractor']['field'] = 'contractor';
  $handler->display->display_options['fields']['contractor']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['contractor']['exclude'] = TRUE;
  /* Field: Entity translation: Назва: translated */
  $handler->display->display_options['fields']['field_title_et']['id'] = 'field_title_et';
  $handler->display->display_options['fields']['field_title_et']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title_et']['field'] = 'field_title_et';
  $handler->display->display_options['fields']['field_title_et']['relationship'] = 'product';
  $handler->display->display_options['fields']['field_title_et']['label'] = 'Продукт';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'product';
  $handler->display->display_options['fields']['title']['label'] = 'Номер';
  /* Field: Content: Бренд */
  $handler->display->display_options['fields']['field_brand']['id'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['table'] = 'field_data_field_brand';
  $handler->display->display_options['fields']['field_brand']['field'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['relationship'] = 'product';
  $handler->display->display_options['fields']['field_brand']['settings'] = array(
    'link' => 0,
  );
  /* Field: Product reference: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'product_reference';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['label'] = 'Кількість';
  /* Field: Link to edit */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Link to edit';
  $handler->display->display_options['fields']['nothing']['label'] = 'Редагувати';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Редагувати';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/[contractor]/product-reference/[prid]/edit';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Product reference: Contractor ID */
  $handler->display->display_options['arguments']['contractor']['id'] = 'contractor';
  $handler->display->display_options['arguments']['contractor']['table'] = 'product_reference';
  $handler->display->display_options['arguments']['contractor']['field'] = 'contractor';
  $handler->display->display_options['arguments']['contractor']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['contractor']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['contractor']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['contractor']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['contractor']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['contractor']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['contractor']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['contractor']['validate_options']['types'] = array(
    'contractor' => 'contractor',
  );
  $handler->display->display_options['path'] = 'node/%/products';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Продукти';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Search for products */
  $handler = $view->new_display('panel_pane', 'Search for products', 'search_for_products');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['link_display'] = FALSE;
  $handler->display->display_options['link_display'] = 'custom_url';
  $handler->display->display_options['link_url'] = 'home';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Product reference: Product ID */
  $handler->display->display_options['relationships']['product']['id'] = 'product';
  $handler->display->display_options['relationships']['product']['table'] = 'product_reference';
  $handler->display->display_options['relationships']['product']['field'] = 'product';
  $handler->display->display_options['relationships']['product']['required'] = TRUE;
  /* Relationship: Product reference: Contractor ID */
  $handler->display->display_options['relationships']['contractor']['id'] = 'contractor';
  $handler->display->display_options['relationships']['contractor']['table'] = 'product_reference';
  $handler->display->display_options['relationships']['contractor']['field'] = 'contractor';
  $handler->display->display_options['relationships']['contractor']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Product reference: Product reference ID (prid) */
  $handler->display->display_options['fields']['prid']['id'] = 'prid';
  $handler->display->display_options['fields']['prid']['table'] = 'product_reference';
  $handler->display->display_options['fields']['prid']['field'] = 'prid';
  $handler->display->display_options['fields']['prid']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['prid']['exclude'] = TRUE;
  /* Field: Product reference: Contractor ID */
  $handler->display->display_options['fields']['contractor']['id'] = 'contractor';
  $handler->display->display_options['fields']['contractor']['table'] = 'product_reference';
  $handler->display->display_options['fields']['contractor']['field'] = 'contractor';
  $handler->display->display_options['fields']['contractor']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['contractor']['exclude'] = TRUE;
  /* Field: Product reference: Product ID */
  $handler->display->display_options['fields']['product']['id'] = 'product';
  $handler->display->display_options['fields']['product']['table'] = 'product_reference';
  $handler->display->display_options['fields']['product']['field'] = 'product';
  $handler->display->display_options['fields']['product']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['product']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'product';
  $handler->display->display_options['fields']['title']['label'] = 'Номер';
  /* Field: Content: Назва */
  $handler->display->display_options['fields']['field_title']['id'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title']['field'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['relationship'] = 'product';
  $handler->display->display_options['fields']['field_title']['label'] = 'Продукт';
  /* Field: Content: Бренд */
  $handler->display->display_options['fields']['field_brand']['id'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['table'] = 'field_data_field_brand';
  $handler->display->display_options['fields']['field_brand']['field'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['relationship'] = 'product';
  $handler->display->display_options['fields']['field_brand']['settings'] = array(
    'link' => 0,
  );
  /* Field: Product reference: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'product_reference';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['label'] = 'Кількість (шт.)';
  /* Field: Product reference: Price */
  $handler->display->display_options['fields']['price']['id'] = 'price';
  $handler->display->display_options['fields']['price']['table'] = 'product_reference';
  $handler->display->display_options['fields']['price']['field'] = 'price';
  $handler->display->display_options['fields']['price']['label'] = 'Ціна (грн.)';
  $handler->display->display_options['fields']['price']['separator'] = ' ';
  /* Field: Entity translation: ПІП, або Назва Підприємства: translated */
  $handler->display->display_options['fields']['title_field_et']['id'] = 'title_field_et';
  $handler->display->display_options['fields']['title_field_et']['table'] = 'field_data_title_field';
  $handler->display->display_options['fields']['title_field_et']['field'] = 'title_field_et';
  $handler->display->display_options['fields']['title_field_et']['relationship'] = 'contractor';
  $handler->display->display_options['fields']['title_field_et']['label'] = 'Контрагент';
  $handler->display->display_options['fields']['title_field_et']['type'] = 'title_linked';
  $handler->display->display_options['fields']['title_field_et']['settings'] = array(
    'title_style' => '_none',
    'title_link' => 'content',
    'title_class' => '',
  );
  /* Field: Лінк на створення продажу */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Лінк на створення продажу';
  $handler->display->display_options['fields']['nothing']['label'] = 'Продаж';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Створити новий продаж';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/add/trade/?prid=[prid]';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Пошук по номеру */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['relationship'] = 'product';
  $handler->display->display_options['filters']['title']['ui_name'] = 'Пошук по номеру';
  $handler->display->display_options['filters']['title']['operator'] = 'word';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Номер продукту';
  $handler->display->display_options['filters']['title']['expose']['description'] = 'Можна шукати кілька номерів розділяючи їх пробілом';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'number';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 'path_override';
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;

  /* Display: Add to trade */
  $handler = $view->new_display('page', 'Add to trade', 'add_to_trade');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Додатий новий продукт до продажу';
  $handler->display->display_options['defaults']['link_display'] = FALSE;
  $handler->display->display_options['link_display'] = 'custom_url';
  $handler->display->display_options['link_url'] = 'home';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Product reference: Product ID */
  $handler->display->display_options['relationships']['product']['id'] = 'product';
  $handler->display->display_options['relationships']['product']['table'] = 'product_reference';
  $handler->display->display_options['relationships']['product']['field'] = 'product';
  $handler->display->display_options['relationships']['product']['required'] = TRUE;
  /* Relationship: Product reference: Contractor ID */
  $handler->display->display_options['relationships']['contractor']['id'] = 'contractor';
  $handler->display->display_options['relationships']['contractor']['table'] = 'product_reference';
  $handler->display->display_options['relationships']['contractor']['field'] = 'contractor';
  $handler->display->display_options['relationships']['contractor']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Product reference: Product reference ID (prid) */
  $handler->display->display_options['fields']['prid']['id'] = 'prid';
  $handler->display->display_options['fields']['prid']['table'] = 'product_reference';
  $handler->display->display_options['fields']['prid']['field'] = 'prid';
  $handler->display->display_options['fields']['prid']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['prid']['exclude'] = TRUE;
  /* Field: Trade ID */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['ui_name'] = 'Trade ID';
  $handler->display->display_options['fields']['php']['label'] = 'excluded from view';
  $handler->display->display_options['fields']['php']['exclude'] = TRUE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_value'] = 'return arg(1);';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'product';
  $handler->display->display_options['fields']['title']['label'] = 'Номер';
  /* Field: Content: Назва */
  $handler->display->display_options['fields']['field_title']['id'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title']['field'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['relationship'] = 'product';
  $handler->display->display_options['fields']['field_title']['label'] = 'Продукт';
  /* Field: Content: Бренд */
  $handler->display->display_options['fields']['field_brand']['id'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['table'] = 'field_data_field_brand';
  $handler->display->display_options['fields']['field_brand']['field'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['relationship'] = 'product';
  $handler->display->display_options['fields']['field_brand']['settings'] = array(
    'link' => 0,
  );
  /* Field: Product reference: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'product_reference';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['label'] = 'Кількість (шт.)';
  /* Field: Product reference: Price */
  $handler->display->display_options['fields']['price']['id'] = 'price';
  $handler->display->display_options['fields']['price']['table'] = 'product_reference';
  $handler->display->display_options['fields']['price']['field'] = 'price';
  $handler->display->display_options['fields']['price']['label'] = 'Ціна (грн.)';
  /* Field: Entity translation: ПІП, або Назва Підприємства: translated */
  $handler->display->display_options['fields']['title_field_et']['id'] = 'title_field_et';
  $handler->display->display_options['fields']['title_field_et']['table'] = 'field_data_title_field';
  $handler->display->display_options['fields']['title_field_et']['field'] = 'title_field_et';
  $handler->display->display_options['fields']['title_field_et']['relationship'] = 'contractor';
  $handler->display->display_options['fields']['title_field_et']['label'] = 'Контрагент';
  /* Field: Лінк на додавання до продажу */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Лінк на додавання до продажу';
  $handler->display->display_options['fields']['nothing']['label'] = 'Продаж';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Додати до продажу';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/[php]/edit/?prid=[prid]';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Пошук по номеру */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['relationship'] = 'product';
  $handler->display->display_options['filters']['title']['ui_name'] = 'Пошук по номеру';
  $handler->display->display_options['filters']['title']['operator'] = 'word';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Номер продукту';
  $handler->display->display_options['filters']['title']['expose']['description'] = 'Можна шукати кілька номерів розділяючи їх пробілом';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'number';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  $handler->display->display_options['path'] = 'node/%/add-to-trade';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Додати до продажу';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['entity_product_reference'] = array(
    t('Master'),
    t('Продукти'),
    t('more'),
    t('Примінити'),
    t('Скинути'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Нічого не знайдено'),
    t('PRID'),
    t('.'),
    t(','),
    t('Contractor products list'),
    t('Продукти Контрагента'),
    t('Product Node'),
    t('excluded from view'),
    t('Продукт'),
    t('Номер'),
    t('Бренд'),
    t('Кількість'),
    t('Редагувати'),
    t('All'),
    t('Search for products'),
    t('Contractor Node'),
    t('Кількість (шт.)'),
    t('Ціна (грн.)'),
    t(' '),
    t('Контрагент'),
    t('Продаж'),
    t('Створити новий продаж'),
    t('Номер продукту'),
    t('Можна шукати кілька номерів розділяючи їх пробілом'),
    t('View panes'),
    t('Add to trade'),
    t('Додатий новий продукт до продажу'),
    t('Додати до продажу'),
  );
  $export['entity_product_reference'] = $view;

  return $export;
}
