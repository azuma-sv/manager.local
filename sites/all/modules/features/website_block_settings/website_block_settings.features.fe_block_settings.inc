<?php
/**
 * @file
 * website_block_settings.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function website_block_settings_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['panels_mini-main_menu'] = array(
    'cache' => -1,
    'css_class' => 'pane-style-default pane-block-site-menu',
    'custom' => 0,
    'delta' => 'main_menu',
    'module' => 'panels_mini',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'azuma_theme' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'azuma_theme',
        'weight' => -9,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['panels_mini-site_branding'] = array(
    'cache' => -1,
    'css_class' => 'pane-style-default pane-site-branding clearfix',
    'custom' => 0,
    'delta' => 'site_branding',
    'module' => 'panels_mini',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'azuma_theme' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'azuma_theme',
        'weight' => -11,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
