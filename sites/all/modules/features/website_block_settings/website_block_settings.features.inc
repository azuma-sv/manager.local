<?php
/**
 * @file
 * website_block_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function website_block_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
