<?php
/**
 * @file
 * Views hooks.
 */

/**
 * Implementation of hook_views_data().
 */
function product_reference_views_data() {
  $data = array();

  // Field base definition.
  $data['product_reference']['table']['group'] = t('Product reference');
  $data['product_reference']['table']['base'] = array(
    'title' => t('Product reference'),
    'help' => t('Contain data related with Product references'),
  );

  // Relation with product node.
  $data['product_reference']['table']['join']['product'] = array(
    'left_field' => 'nid',
    'field' => 'product',
  );
  // Relation with contractor node.
  $data['product_reference']['table']['join']['contractor'] = array(
    'left_field' => 'nid',
    'field' => 'contractor',
  );

  // Primary field PRID.
  $data['product_reference']['prid'] = array(
    'title' => t('Product reference ID (prid)'),
    'help' => t('The record ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Product NID.
  $data['product_reference']['product'] = array(
    'title' => t('Product ID'),
    'help' => t('NID of the referenced product.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'product',
      'handler' => 'views_handler_relationship',
      'label' => t('Product Node'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  // Contractor NID.
  $data['product_reference']['contractor'] = array(
    'title' => t('Contractor ID'),
    'help' => t('NID of the referenced contractor.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'contractor',
      'handler' => 'views_handler_relationship',
      'label' => t('Contractor Node'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  // Field amount.
  $data['product_reference']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('Products amount.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Field price.
  $data['product_reference']['price'] = array(
    'title' => t('Price'),
    'help' => t('Product price.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}
