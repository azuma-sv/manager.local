<?php
/**
 * @file
 * Admin pages functionality.
 */

/**
 * Callback of page to edit product reference entity.
 */
function product_reference_page_edit($contractor, $product_reference) {
  // Load product form.
  if (!empty($product_reference)) {
    return drupal_get_form('product_reference_form', $product_reference);
  }

  // Not found.
  return drupal_not_found();
}

/**
 * Callback of form to create/edit product reference entity.
 */
function product_reference_form($form, &$form_state, $product_reference) {
  // Build form fields.
  $form = $form + product_reference_form_fields($product_reference);
  // Submit trigger.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  // @TODO: Decide what to do with deletion.
  // Link to remove entity.
  // if (!empty($product_reference->prid)) {
  //   $form['delete'] = array(
  //     '#type' => 'link',
  //     '#title' => t('Delete'),
  //     '#href' => 'product_reference/' . $product_reference->prid . '/delete',
  //     '#weight' => 200,
  //   );
  // }
  return $form;
}

/**
 * Form validation handler: Validates product_reference_form information.
 */
function product_reference_form_validate($form, &$form_state) {
  // Fetch and update fields.
  $values = product_reference_form_fields_fetch($form_state['values']);
  foreach (product_reference_fields() as $field => $title) {
    if (empty($values[$field]) && $field != 'amount') {
      form_set_error($field, t('Field !field can\'t be empty.', array('!field' => $title)));
    }
  }

  // Load referenced nodes for validation.
  $contractor = $values['contractor'];
  $product = $values['product'];
  $nodes = node_load_multiple(array_filter(array($contractor, $product)));

  // Validate contractor.
  if (empty($nodes[$contractor]) || $nodes[$contractor]->type != 'contractor') {
    form_error($form['contractor'], t('There is no contractor with ID: "!id".', array('!id' => $contractor)));
  }

  // Validate product.
  if (empty($nodes[$product]) || $nodes[$product]->type != 'product') {
    form_error($form['product'], t('There is no product with ID: "!id".', array('!id' => $product)));
  }

  // If current reference will be a new one.
  if (empty($values['prid'])) {
    // We can't add two same products to one contractor.
    $product_reference = product_reference_conditional_load($contractor, $product);
    if (!empty($product_reference)) {
      // Error message.
      $replacements = array(
        '!field' => $title,
        '!edit' => l(t('Edit existing'), 'node/' . $contractor . '/product-reference/' . $product_reference->prid . '/edit'),
      );
      form_set_error('product', t('Can\'t add two same products to one contractor. !edit', $replacements));
    }
  }
}

/**
 * Form submit handler: Submits product_reference_form information.
 */
function product_reference_form_submit($form, &$form_state) {
  // Fetch base entity.
  $product_reference = $form_state['values']['product_reference'];

  // Fetch and update fields.
  $values = product_reference_form_fields_fetch($form_state['values']);
  foreach (product_reference_fields() as $field => $title) {
    $product_reference->{$field} = isset($values[$field]) ? $values[$field] : '';
  }

  // Save entity and redirect to view page.
  product_reference_save($product_reference);
  $form_state['redirect'] = 'node/' . $product_reference->contractor . '/products';
}

/**
 * Callback of page to remove product reference entity.
 * @todo: Decide what to do with deletion.
 */
function product_reference_page_delete($product_reference) {
  // Load product form.
  if (!empty($product_reference)) {
    return drupal_get_form('product_reference_delete_form', $product_reference);
  }

  // Not found.
  return drupal_not_found();
}

/**
 * Form function to remove Product reference entity.
 */
function product_reference_delete_form($form, &$form_state, $product_reference) {
  $form = array();

  // Page title.
  drupal_set_title(t('Remove product reference: !ID', array('!ID' => $product_reference->prid)));

  // Hidden entity.
  $form['product_reference'] = array(
    '#type' => 'value',
    '#value' => $product_reference,
  );

  // Confirm form building.
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => t('Product reference with id !ID', array('!ID' => $product_reference->prid)))),
    'product_reference/' . $product_reference->prid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Form submit for "product_reference_delete_form".
 */
function product_reference_delete_form_submit($form, &$form_state) {
  // Fetch entity.
  $product_reference = $form_state['values']['product_reference'];
  // Set proper redirect.
  if (!empty(!empty($product_reference->contractor))) {
    $form_state['redirect'] = 'node/' . $product_reference->contractor . '/products';
  }
  else {
    $form_state['redirect'] = '<front>';
  }
  // Remove entity.
  product_reference_delete($product_reference->prid);
}
