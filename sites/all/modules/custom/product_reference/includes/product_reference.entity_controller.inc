<?php
/**
 * @file
 * Product reference entity controller.
 */

/**
 * EntityProductReferenceControllerInterface definition.
 */
interface EntityProductReferenceControllerInterface
  extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();

  /**
   * Save an entity.
   *
   * @param object $entity
   *   The entity to save.
   */
  public function save($product_reference);

  /**
   * Delete an entity.
   *
   * @param object $entity
   *   The entity to delete.
   */
  public function delete($product_reference);

  /**
   * Delete an entity multiple.
   *
   * @param object $entity
   *   The entity to delete.
   */
  public function deleteMultiple($product_references);
}

/**
 * EntityProductReferenceController extends DrupalDefaultEntityController.
 */
class EntityProductReferenceController
  extends DrupalDefaultEntityController
  implements EntityProductReferenceControllerInterface {

  /**
   * Create and return a new empty entity.
   */
  public function create() {
    $product_reference = new stdClass();
    $product_reference->type = PRODUCT_REFERENCE_DEFAULT_BUNDLE;
    $product_reference->prid = NULL;
    return $product_reference;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($product_reference) {
    $primary_keys = isset($product_reference->prid) ? 'prid' : array();
    // Bundle validation to prevent errors.
    if (empty($product_reference->type)) {
      $product_reference->type = PRODUCT_REFERENCE_DEFAULT_BUNDLE;
    }
    // Write out the entity record.
    drupal_write_record('product_reference', $product_reference, $primary_keys);
    return $product_reference;
  }

  /**
   * Delete a single entity.
   */
  public function delete($product_reference) {
    $this->deleteMultiple(array($product_reference));
  }

  /**
   * Delete one or more entity_example_basic entities.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($product_references) {
    if (!empty($product_references)) {
      // @TODO: Decide what to do with deletion.
      // $prids = array();
      // foreach ($product_references as $product_reference) {
      //   if (isset($product_reference->prid)) {
      //     $prids[] = $product_reference->prid;
      //   }
      // }
      // // If we have something to delete.
      // if (!empty($prids)) {
      //   db_delete('product_reference')
      //     ->condition('prid', $prids, 'IN')
      //     ->execute();
      // }
    }
  }
}
