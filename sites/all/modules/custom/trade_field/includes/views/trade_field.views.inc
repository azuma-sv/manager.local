<?php
/**
 * @file
 * Views hooks.
 */

/**
 * Implementation of hook_views_data_alter().
 * @todo: Fix this mess. It shouldn't be attached to the field created via drupal interface.
 * We need to define own views data definitions.
 */
function trade_field_views_data_alter(&$data) {
  // Contractor field and relationship handlers.
  $data['field_data_field_trade']['field_trade_contractor']['field']['handler'] = 'views_handler_field_node';
  $data['field_data_field_trade']['field_trade_contractor']['relationship'] = array(
    'base' => 'node',
    'field' => 'field_trade_contractor',
    'handler' => 'views_handler_relationship',
    'label' => t('Contractor Node'),
  );

  // Product field and relationship handlers.
  $data['field_data_field_trade']['field_trade_product']['field']['handler'] = 'views_handler_field_node';
  $data['field_data_field_trade']['field_trade_product']['relationship'] = array(
    'base' => 'node',
    'field' => 'field_trade_product',
    'handler' => 'views_handler_relationship',
    'label' => t('Product Node'),
  );

  // Add other fields.
  $data['field_data_field_trade']['field_trade_price_seller']['field']['handler'] = 'views_handler_field_numeric';
  $data['field_data_field_trade']['field_trade_price_buyer']['field']['handler'] = 'views_handler_field_numeric';
  $data['field_data_field_trade']['field_trade_amount']['field']['handler'] = 'views_handler_field_numeric';
}
