<?php
/**
 * @file
 * Administration pages functionality.
 */

/**
 * Page callback to add product to contractor.
 */
function azuma_page_contractor_add_product($node) {
  // Create empty entity to insert into the form.
  $product_reference = entity_get_controller('product_reference')->create();
  $product_reference->contractor = $node->nid;
  // Build form.
  module_load_include('inc', 'product_reference', 'includes/product_reference.admin');
  return drupal_get_form('product_reference_form', $product_reference);
}

/**
 * Page callback to provide functionality of autocomplete fields for nodes.
 */
function azuma_page_autocomplete_node_fields($string) {
  // Fetch arguments.
  $bundle = (arg(2) == 'all') ? NULL : arg(2);
  $type = arg(3);
  $amount = arg(4);
  $results = array();
  $return = array();
  $field_id = 'nid';
  $field_label = 'title';

  // Chose proper query type.
  switch ($type) {
    case 'title':
      switch ($bundle) {
        // Multilingual titles.
        case 'contractor':
          // Fetch node titles.
          $results = db_select('field_data_title_field', 'fdtf');
          $results->fields('fdtf', array('entity_id', 'title_field_value'));
          $results = $results
            ->condition('fdtf.entity_type', 'node')
            ->condition('fdtf.bundle', $bundle)
            ->condition('fdtf.deleted', FALSE)
            ->condition('fdtf.title_field_value', '%' . db_like($string) . '%', 'LIKE')
            ->orderBy('entity_id', 'DESC')
            ->range(0, $amount)
            ->execute();
          // Change field names.
          $field_id = 'entity_id';
          $field_label = 'title_field_value';
          break;

        // Not translatable titles.
        default:
          // Fetch node titles.
          $results = db_select('node', 'n');
          $results->fields('n', array('nid', 'title'));
          if (isset($bundle)) {
            $results->condition('n.type', $bundle);
          }
          $results = $results
            ->condition('n.title', '%' . db_like($string) . '%', 'LIKE')
            ->range(0, $amount)
            ->execute();
          break;
      }
      break;
  }

  // Format data.
  foreach ($results as $row) {
    $value = azuma_wrap_id_for_autocomplete_page($row->{$field_label}, $row->{$field_id});
    $return[$value] = $value;
  }

  drupal_json_output($return);
}

/**
 * Callback for page "taxonomy/%taxonomy_vocabulary_machine_name/add".
 */
function azuma_taxonomy_term_add_form($form, &$form_state, $vocabulary) {
  // Page title.
  $vocabulary_name = i18n_taxonomy_vocabulary_name($vocabulary);
  drupal_set_title(t('Create @name', array('@name' => $vocabulary_name)));
  // Load form.
  form_load_include($form_state, 'inc', 'taxonomy', 'taxonomy.admin');
  $form = taxonomy_form_term($form, $form_state, array(), $vocabulary);
  $form['#submit'][] = 'taxonomy_form_term_submit';
  // Hide unwanted fields.
  $form['relations']['#access'] = false;
  $form['description']['#access'] = false;
  // Return form.
  return $form;
}
