<?php
/**
 * @file
 * Import functionality.
 */

/**
 * Form callback for import.
 * @todo: Import can become better.
 */
function azuma_import_form($form, &$form_state) {
  if (!isset($form_state['storage']['step'])) {
    $form_state['storage']['step'] = 'parse_import';
    // Tip for managers.
    $form['tip-label'] = array(
      '#markup' => t('Columns order example'),
    );
    $form['tip'] = array(
      '#theme' => 'table',
      '#header' => array(t('Brand'), t('Number'), t('Name'), t('Amount'), t('Price')),
    );
    // Contractors.
    $contractors = db_select('node', 'n');
    $contractors = $contractors
      ->fields('n', array('nid', 'title'))
      ->condition('type', 'contractor')
      ->condition('status', TRUE)
      ->execute()
      ->fetchAllKeyed();
    $form['contractor'] = array(
      '#title' => t('Contractor'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#autocomplete_path' => 'autocomplete/node/contractor/title/15',
    );
    // Text to ignore table header.
    $form['ignore'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ignore header (Ignore first table row)'),
      '#default_value' => TRUE,
    );
    // Textarea to import csv file.
    $form['import'] = array(
      '#type' => 'file',
      '#title' => t('File .CSV'),
    );
    // Submit.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Preview'),
    );
    return $form;
  }

  $form_state['storage']['step'] = 'submit_import';
  // Wrapper to tell which brands will be created.
  $form['brands_to_create'] = array(
    '#type' => 'fieldset',
    '#title' => t('Brands which will be inserted'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // List brands to create.
  if (!empty($form_state['storage']['brands_to_create'])) {
    $brands_to_create = $form_state['storage']['brands_to_create'];
    $form['brands_to_create']['#title'] .= ' - ' . t('(!amount) items.', array('!amount' => count($brands_to_create)));
    foreach (array_keys($form_state['storage']['brands_to_create']) as $brand) {
      $form['brands_to_create'][] = array(
        '#theme' => 'html_tag',
        '#tag' => 'div',
        '#value' => $brand,
      );
    }
  }
  else {
    $form['brands_to_create']['#collapsed'] = FALSE;
    $form['brands_to_create'][] = array(
      '#theme' => 'html_tag',
      '#tag' => 'div',
      '#value' => t('Nothing'),
    );
  }

  // List products to create.
  $form['products_to_create'] = array(
    '#type' => 'fieldset',
    '#title' => t('Products which will be inserted'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if (!empty($form_state['storage']['products_to_create'])) {
    $products_to_create = $form_state['storage']['products_to_create'];
    $form['products_to_create']['#title'] .= ' - ' . t('(!amount) items.', array('!amount' => count($products_to_create)));
    foreach ($form_state['storage']['products_to_create'] as $product) {
      $form['products_to_create'][] = array(
        '#theme' => 'html_tag',
        '#tag' => 'div',
        '#value' => '[' . $product['number'] . '] - ' . $product['name'],
      );
    }
  }
  else {
    $form['products_to_create']['#collapsed'] = FALSE;
    $form['products_to_create'][] = array(
      '#theme' => 'html_tag',
      '#tag' => 'div',
      '#value' => t('Nothing'),
    );
  }
  // Submit.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Form validation handler.
 */
function azuma_import_form_validate($form, &$form_state) {
  if ($form_state['storage']['step'] == 'parse_import') {
    $file = file_save_upload('import', array(
      // Validate extensions.
      'file_validate_extensions' => array('csv'),
    ));
    // If the file passed validation:
    if ($file) {
      // Move the file into the Drupal file system.
      if ($file = file_move($file, 'public://')) {
        // Save the file for use in the submit handler.
        $form_state['storage']['import'] = $file;
      }
      else {
        form_set_error('import', t('Failed to write the uploaded file to the site\'s file folder.'));
      }
    }
    else {
      form_set_error('import', t('No file was uploaded.'));
    }
  }
}


/**
 * Form submit handler.
 */
function azuma_import_form_submit($form, &$form_state) {
  global $user;
  if ($form_state['storage']['step'] == 'parse_import') {
    if (!empty($form_state['storage']['import'])) {
      $import = $form_state['storage']['import'];
      // Read file.
      $drupal_filepath = drupal_realpath($import->uri);
      $file_handler = fopen($drupal_filepath, "r");
      $csv_content = fread($file_handler, filesize($drupal_filepath));
      fclose($file_handler);
    }

    if (!empty($csv_content)) {
      $skip = isset($form_state['values']['ignore']) ? $form_state['values']['ignore'] : TRUE;
      // Prepare vocabulary.
      $vocabulary_brand = taxonomy_vocabulary_machine_name_load('brand');
      $taxonomy_brand = taxonomy_get_tree($vocabulary_brand->vid);
      $terms_brand = array();
      foreach ($taxonomy_brand as $term) {
        $terms_brand[$term->name] = $term->tid;
      }

      // Prepare variables.
      $results = array();
      $index = 0;
      $csv_products = $csv_product_names = $brands_to_create = array();
      // Explode to lines in the file.
      foreach (explode(PHP_EOL, $csv_content) as $row_id => $line) {
        // Skip empty rows (just validation).
        $line = trim($line);
        if (empty($line)) {
          continue;
        }

        // Skip first row if configured.
        if ($skip && $index == 0) {
          $index++;
          continue;
        }

        // Prepare values.
        $items = explode(',', $line);
        foreach ($items as $key => $item) {
          $items[$key] = trim($item);
        }

        // Extract data.
        list($brand, $number, $name, $amount, $price) = $items;
        $number = azuma_prepare_product_title($number);
        $results[$row_id] = array();

        // Processing brand names.
        if (isset($terms_brand[$brand])) {
          // Use existing if we can.
          $results[$row_id]['brand'] = $terms_brand[$brand];
        }
        else {
          // Create new one.
          $brands_to_create[$brand][] = $row_id;
        }

        // Processing products.
        $csv_products[$row_id] = $number;
        $csv_product_names[$row_id] = $name;
        $results[$row_id]['amount'] = $amount;
        $results[$row_id]['price'] = $price;

        // Index step.
        $index++;
      }
    }

    // Products check.
    $products_to_create = array();
    $products = db_select('node', 'n');
    $products = $products
      ->fields('n', array('nid', 'title'))
      ->condition('type', 'product')
      ->condition('title', $csv_products, 'IN')
      ->execute()
      ->fetchAllKeyed(1, 0);
    foreach ($csv_products as $row_id => $product_number) {
      if (!empty($products[$product_number])) {
        $results[$row_id]['product'] = $products[$product_number];
      }
      else {
        $products_to_create[$row_id]['number'] = $product_number;
        $products_to_create[$row_id]['name'] = $csv_product_names[$row_id];
      }
    }
    $form_state['storage']['products_to_create'] = $products_to_create;

    // What brands will be created.
    if (!empty($brands_to_create)) {
      $form_state['storage']['brands_to_create'] = $brands_to_create;
    }
    // Results.
    $form_state['storage']['results'] = $results;
    $form_state['storage']['contractor'] = $form_state['values']['contractor'];
    $form_state['rebuild'] = TRUE;
  }

  if ($form_state['storage']['step'] == 'submit_import') {
    // Create terms.
    if (!empty($form_state['storage']['brands_to_create'])) {
      $vocabulary_brand = taxonomy_vocabulary_machine_name_load('brand');
      foreach ($form_state['storage']['brands_to_create'] as $brand => $row_ids) {
        $term = new stdClass();
        $term->name = $brand;
        $term->vid = $vocabulary_brand->vid;
        $term->tid = NULL;
        $term->language = 'uk';
        taxonomy_term_save($term);
        foreach ($row_ids as $row_id) {
          $form_state['storage']['results'][$row_id]['brand'] = $term->tid;
        }
      }
    }

    // Create products.
    if (!empty($form_state['storage']['products_to_create'])) {
      foreach ($form_state['storage']['products_to_create'] as $row_id => $product) {
        $node = entity_create('node', array('type' => 'product'));
        $node->uid = $user->uid;
        $node->title = $product['number'];
        $node->language = 'uk';
        $node_wrapper = entity_metadata_wrapper('node', $node);
        $node_wrapper->field_title->set($product['name']);
        $node_wrapper->field_brand->set($form_state['storage']['results'][$row_id]['brand']);
        $node_wrapper->save();
        $form_state['storage']['results'][$row_id]['product'] = $node->nid;
      }
    }

    // Contractor settings.
    $contractor = $form_state['storage']['contractor'];
    $contractor = azuma_fetch_id_from_autocomplete_page($contractor);
    $product_references = product_reference_load_multiple(array(), array('contractor' => $contractor));
    // Check if we need to update something.
    foreach ($product_references as $product_reference) {
      // Compare.
      foreach ($form_state['storage']['results'] as $row_id => $data) {
        if ($product_reference->product == $data['product']) {
          $product_reference->price = $data['price'];
          $product_reference->amount = $data['amount'];
          product_reference_save($product_reference);
          unset($form_state['storage']['results'][$row_id]);
        }
      }
    }
    if (!empty($form_state['storage']['results'])) {
      foreach ($form_state['storage']['results'] as $data) {
        product_reference_create($contractor, $data['product'], $data['amount'], $data['price']);
      }
    }
  }
}
